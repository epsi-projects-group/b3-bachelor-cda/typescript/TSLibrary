import { Adherent } from './class/Adherent';
import { BandeDessine } from './class/BandeDessine';
import { Livre } from './class/Livre';
import { Bibliotheque } from './class/Bibliotheque';

// Instantiate volumes
const luckyLuck = new BandeDessine(1, "Lucky Luck", new Date(Date.now()), "Hugo Robin", "John Cena");
const maladeImaginaire = new Livre(2, "Malade Imaginaire", new Date(Date.now()), "Molière", "123-6763-7876");
const titeuf = new BandeDessine(3, "Titeuf", new Date(Date.now()), "Zep", "1232-123-123");

// Set nombre exemplaires
maladeImaginaire.setNombreExemplaire(0);
luckyLuck.setNombreExemplaire(2);
titeuf.setNombreExemplaire(0);

// Add bibliothèque
const biblio = new Bibliotheque();

// Abonnement à l’événement (Fire Event)
biblio.empruntEmisAnnonce$.subscribe((volume) => {
    console.log(`Le volume ${volume.getTitre()} a été emprunté.`);
});

// Ajout adherents
const robertoRodriguez = new Adherent(0, "Roberto", "Rodriguez");
const hugoRobin = new Adherent(1, "Robin", "Hugo");
biblio.ajouterAdherent(robertoRodriguez);
biblio.ajouterAdherent(hugoRobin);

// Ajout ouvrages
biblio.ajouterOuvrage(maladeImaginaire);
biblio.ajouterOuvrage(luckyLuck);
biblio.ajouterOuvrage(titeuf);

// Emprunt (va émettre un événement et donc afficher un message dans la console)
biblio.emprunte(hugoRobin, luckyLuck);

