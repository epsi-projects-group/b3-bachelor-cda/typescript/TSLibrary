import { Volume } from "./Volume";

export class Livre extends Volume {
  private _isbn: string;

  constructor(
    id: number,
    titre: string,
    dateParution: Date,
    auteur: string,
    isbn: string
  ) {
    super(id, titre, dateParution, auteur);
    this._isbn = isbn;
  }

  getIsbn(): string {
    return this._isbn;
  }

  description(): string {
    return `Livre: ${this.getId()}, Titre: ${this.getTitre()}, Date parution: ${this.getDateParution()} Auteur: ${this.getAuteur()}, Isbn: ${this.getIsbn()}`;
  }
}
