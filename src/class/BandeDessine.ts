import { Volume } from "./Volume";

export class BandeDessine extends Volume {
  private _nomScenariste: string;

  constructor(
    id: number,
    titre: string,
    dateParution: Date,
    nomDessinateur: string,
    nomScenariste: string
  ) {
    super(id, titre, dateParution, nomDessinateur);
    this._nomScenariste = nomScenariste;
  }

  getScenariste(): string {
    return this._nomScenariste;
  }

  description(): string {
    return `Bande dessiné: ${this.getId()}, Titre: ${this.getTitre()},  Date parution: ${this.getDateParution()}, Dessinateur: ${this.getAuteur()}, Scenariste: ${this.getScenariste()}`;
  }
}
