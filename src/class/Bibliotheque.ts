import { Adherent } from "./Adherent";
import { Volume } from "./Volume";
import { Emprunt } from "./Emprunt";
import { Subject, Observable } from "rxjs";

export class Bibliotheque {
  private _adherents: Adherent[] = [];
  private _ouvrages: Volume[] = [];
  private _subjectEmpruntEmis = new Subject<Volume>();
  empruntEmisAnnonce$: Observable<Volume>;

  constructor() {
    this.empruntEmisAnnonce$ = this._subjectEmpruntEmis.asObservable();
  }

  ajouterAdherent(adherent: Adherent): boolean {
    this._adherents.push(adherent);
    return true;
  }

  ajouterOuvrage(ouvrage: Volume): boolean {
    this._ouvrages.push(ouvrage);
    return true;
  }

  supprimerOuvrage(id: number): boolean {
    let indexOuvrageASupprimer = this._ouvrages.findIndex(
      (ouvrage) => ouvrage.getId() === id
    );
    if (indexOuvrageASupprimer >= 0) {
      this._ouvrages.splice(indexOuvrageASupprimer, 1);
      return true;
    }
    return false;
  }

  supprimerAdherent(id: number): boolean {
    let adherentASupprimer = this._adherents.find(
      (adherent) => adherent.getId() === id
    );
    let indexAdherentASupprimer = this._adherents.findIndex(
      (adherent) => adherent.getId() === id
    );

    if (indexAdherentASupprimer > -1 && adherentASupprimer?.getEmprunts().length === 0) {
        this._adherents.splice(indexAdherentASupprimer, 1);
        return true;
    }
    return false;
  }

  rechercherAdherent(id: number): boolean {
    let adherentRechercher = this._adherents.find(
      (adherent) => adherent.getId() === id
    );
    if (adherentRechercher) {
      return true;
    }
    return false;
  }

  rechercherOuvrage(id: number): boolean {
    let ouvrageRechercher = this._ouvrages.find(
      (ouvrage) => ouvrage.getId() === id
    );
    if (ouvrageRechercher) {
      return true;
    }
    return false;
  }

  rechercherOuvrageParTitre(titre: string): boolean {
    let ouvrageRechercher = this._ouvrages.find(
      (ouvrage) => ouvrage.getTitre() === titre
    );
    if (ouvrageRechercher) {
      return true;
    }
    return false;
  }

  afficherAdherents(): void {
    this._adherents.forEach((adherent) => {
      console.log(adherent.description());
    });
  }

  afficherOuvrages(): void {
    this._ouvrages.forEach((ouvrage) => {
      console.log(ouvrage.description());
    });
  }

  afficherEmprunteursOuvrages(): void {
    this._adherents.forEach((adherent) => {
      console.log(adherent.description());
      adherent.afficherEmprunts();
    });
  }

  emprunte(adherent: Adherent, ouvrage: Volume): boolean {
    if (
      this.rechercherAdherent(adherent.getId()) &&
      this.rechercherOuvrage(ouvrage.getId()) &&
      adherent.emprunte(ouvrage)
    ) {
      this._subjectEmpruntEmis.next(ouvrage);
      return true;
    }
    return false;
  }
}
