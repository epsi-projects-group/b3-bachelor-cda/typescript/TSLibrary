export interface IOuvrage {
  readonly id: number;
  readonly titre: string;
  readonly dateParution: Date;

  description(): string;
  getId(): number;
  getTitre(): string;
  getDateParution(): Date;
}
