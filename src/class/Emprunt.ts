import { Volume } from "./Volume";
import { Adherent } from "./Adherent";

export class Emprunt {
  private _adherent: Adherent;
  private _volume: Volume;
  private _dateEmprunt: Date;

  constructor(adherent: Adherent, volume: Volume, dateEmprunt: Date) {
    this._adherent = adherent;
    this._volume = volume;
    this._dateEmprunt = dateEmprunt;
  }

  description(): string {
    return `- Volume (${
      this._volume.constructor.name
    }): ${this._volume.getTitre()}, Emprunté par: ${this._adherent.getPatronyme()}, Emprunté le: ${this._dateEmprunt.toUTCString()}`;
  }
}
