import { IOuvrage } from "./IOuvrage";

export abstract class Volume implements IOuvrage {
  id: number;
  titre: string;
  dateParution: Date;
  private _auteur: string;
  private _nombreExemplaire: number = 0;
  private _nombreExemplaireEmprunte: number = 0;

  constructor(id: number, titre: string, dateParution: Date, auteur: string) {
    this.id = id;
    this.titre = titre;
    this.dateParution = dateParution;
    this._auteur = auteur;
  }

  getId(): number {
    return this.id;
  }

  getTitre(): string {
    return this.titre;
  }

  getDateParution(): Date {
    return this.dateParution;
  }

  getAuteur(): String {
    return this._auteur;
  }

  getNombreExemplaire(): number {
    return this._nombreExemplaire;
  }

  getNombreExemplaireEmprunte(): number {
    return this._nombreExemplaireEmprunte;
  }

  setNombreExemplaire(nombreExemplaire: number): void {
    this._nombreExemplaire = nombreExemplaire;
  }

  setNombreExemplaireEmprunte(nombreExemplaire: number): void {
    this._nombreExemplaireEmprunte = nombreExemplaire;
  }

  empruntable(): boolean {
    if (this._nombreExemplaireEmprunte < this._nombreExemplaire) {
      this._nombreExemplaireEmprunte++;
      return true;
    }
    return false;
  }

  abstract description(): string;
}
