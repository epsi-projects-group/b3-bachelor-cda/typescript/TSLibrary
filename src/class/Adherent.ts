import { Volume } from "./Volume";
import { Emprunt } from "./Emprunt";

export class Adherent {
  private _id: number;
  private _nom: string;
  private _prenom: string;
  private _emprunts: Emprunt[] = [];

  constructor(id: number, nom: string, prenom: string) {
    this._id = id;
    this._nom = nom;
    this._prenom = prenom;
  }

  getId(): number {
    return this._id;
  }

  getNom(): string {
    return this._nom;
  }

  getPatronyme(): string {
    return `${this.getNom()} ${this.getPrenom()}`;
  }

  getPrenom(): string {
    return this._prenom;
  }

  getEmprunts(): Emprunt[] {
    return this._emprunts;
  }

  description(): string {
    return `Adherent: ${this.getId()}, Nom: ${this.getNom()}, Prenom: ${this.getPrenom()}`;
  }

  emprunte(volume: Volume): boolean {
    if (volume.empruntable()) {
      this._emprunts.push(new Emprunt(this, volume, new Date(Date.now())));
      return true
    }
    return false
  }

  afficherEmprunts(): void {
    this._emprunts.forEach((emprunt) => {
      console.log(emprunt.description());
    });
  }
}
