import { Livre } from "../src/class/Livre";
import * as assert from "assert"

describe("Livre Tests", () => {

    it("Test de la classe", () => {
        assert.equal(typeof Livre, "function");
    });

    it("Test de l'instanciation et des getters", () => {
        const livre = new Livre(1, "Le petit prince", new Date(1943, 4, 6), "Antoine de Saint-Exupéry", "96");
        assert.equal(livre.getId(), 1);
        assert.equal(livre.getTitre(), "Le petit prince");
        assert.equal(livre.getAuteur(), "Antoine de Saint-Exupéry");
    });

    it("Test de la méthode description", () => {
        const livre = new Livre(1, "Le petit prince", new Date(1943, 4, 6), "Antoine de Saint-Exupéry", "96");
        assert.equal(livre.description(), "Livre: 1, Titre: Le petit prince, Date parution: Thu May 06 1943 00:00:00 GMT+0200 (heure d’été d’Europe centrale) Auteur: Antoine de Saint-Exupéry, Isbn: 96");
    });

});