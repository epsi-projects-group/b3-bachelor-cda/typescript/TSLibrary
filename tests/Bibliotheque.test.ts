import { Bibliotheque } from "../src/class/Bibliotheque";
import * as assert from "assert"
import { Livre } from "../src/class/Livre";
import { Adherent } from "../src/class/Adherent";

describe("Bibliotheque Tests", () => {

    it("Test de l'observable émet event", () => {
        const biblio = new Bibliotheque();
        const livre = new Livre(1, "Le petit prince", new Date(1943, 4, 6), "Antoine de Saint-Exupéry", "96");
        livre.setNombreExemplaire(10);
        const user = new Adherent(1, "Robin", "Hugo");
        let message = "";
        biblio.empruntEmisAnnonce$.subscribe((volume) => {
            message = `Le volume ${volume.getTitre()} a été emprunté.`;
        });
        biblio.ajouterAdherent(user);
        biblio.ajouterOuvrage(livre);
        biblio.emprunte(user, livre);
        assert.equal(message, "Le volume Le petit prince a été emprunté.");
    });

});